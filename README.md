CSC 111 Computer Science II
Project: Stack & Queue Juggler readme
Programmer:  Adam Standke
Professor: Dr. Lee
File Created: November, 22, 2017
File Updated: November, 22, 2017

sqjuggler description: program that reads data from a formatted file and does either an add operation 
		       which inserts a number  into a linked list implementation of a stack/ queue or
		       a delete operation which removes a number from a stack/queue and  inserts
		       it into a differnt queue/stack

Platform Info:     Sloop(Linix Ubuntu)

Programming Language: C language 

Files Included: sqjuggler.c, sqjuggler.h, queue.c, stack.c, makefile

How to compile sqjuggler                1. Type make
	                                2. gcc -o sqjuggler sqjuggler.c sqjuggler.h queue.c stack.c 

How to run sqjuggler:   ./sqjuggler


Known Bugs in sqjuggler: program can only read integer type values, so the length of numbers must be 
			 limited to what an integer type variable is able to store in memory. Failing
			 to do so will cause an integer overflow error to occur. Also, the function fgets
			 stores input in a fixed size character array (named buffer) that supports a maximum
			 amount of 20 bytes of memory. Because the program reads only a text file, and not a
			 binary file, each individual digit of a number is given 1 byte of memory. Therefore
			 one has to take into consideration not only number length but also spacing when the 
			 add operation is called. If too many spaces seperate the add keyword from the actual
			 number to be inserted, the wrong number (or even no number) will be added to a 
			 data structure. Accordingly, the file's contents should be properly formatted, otherwise
			 undefined behaviour will occur. 
