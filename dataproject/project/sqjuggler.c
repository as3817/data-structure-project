/*
Project: Stack & Queue Juggler
Programmer: Adam Standke
Course: CSC 111
Professor: Dr. Lee
*/

#include <stdio.h> 		//directive to include C's std i/o library in file			
#include <stdlib.h>		//directive to include C's std library in file
#include <string.h>		//directive to include C's string library in file
#include <ctype.h>		//directive to include C's character library in file
#include "sqjuggler.h"		//directive to include header file defined in another file

int addOperation(char *, int, int);	//function prototype for a function that looks for numbers in a string 
void print(void); 			//function prototype for a function that prints out outStack and outQueue
int count=0; 				//global variable that keeps track of the number of items pushed/enqueued 

int main (int argc, char *argv[])	//main function where filename will be passed as an argument from cmd line
{

FILE *fp;			//decleration of a pointer variable of FILE type
char buffer[BUFFER_SIZE];	//decleration of char array that will hold 20 bytes of input from a file 
char add[10];			//decleration of a char array that will hold the string add from a file
int temp=0;          		//variable that stores values from add operation 
 		
if(argc !=2)			//checks to see that a filename has been entered on the cmd line
{
	printf("Please enter one filename\n"); 		//will print to std output the following message
	exit(EXIT_FAILURE); 				//exit function will be called terminating program
}
if((fp=fopen(argv[1], "r"))==NULL)			//checks to see if file can be opened
{
	printf("%s can't be opened\n", argv[1]); 	//if file cannot be opend will print to std output message
	exit(EXIT_FAILURE); 				//exit function will be called terminating program
}
while(fgets(buffer, sizeof(buffer), fp))		//fgets function stores data from file in buffer variable
{							//the loop will terminate when fgets returns null 

	if(sscanf(buffer, "%s %d", add, &temp)==2)	//first conditional that uses sscanf to find a pattern
	{						//in the array  buffer looking for an add operation followed
		addInStack(temp); 			//by a number. This conditional checks to see if white
		addInQueue(temp); 			//space characters follow the string argument eg.
		add[0]=0;				//"add   10". If a number is found it is stored in temp and
		count++; 				// passed to both inStack and inQueue
	}
	else if ( sscanf(buffer, "%s", add)==1 && (add[0]=='a' 
		 || add[0]=='A'))				//second conditional statement that uses sscanf to  
	{							//find a pattern in the array buffer looking for
		char *p=NULL;  					//an add operation that is only comprised of a string
		if((p=strpbrk(buffer, "0123456789"))!=NULL)	//such as for example "add45." This conditional will
		{						//will first see if the string is an add operation
			int x = addOperation(p, 0, 0);  	//by checking the first element in the buffer 
			addInStack(x); 				//is either an a or A charcter. Then the function
			addInQueue(x);				//strpbrk is called to see if the string contains a
			add[0]=0; 				//number. If so, the addOperation function is called
			count++; 				//to return an integer number and pass it to both
		}						//inStack and inQueue
		
	}
	else if (buffer[0]=='d' || buffer[0]=='D')		//third conditional statement that checks to see
	{							//that a delete operation is required. This
		int i, j; 					//conditional will see if buffer's first element is
		i=deleteInStack(); 				//either a d or D character. If so, elements from 
		j=deleteInQueue(); 				//inStack and inQueue will be transfered over to
		addOutQueue(i); 				//outStack and outQueue. 
		addOutStack(j); 

	}
	else							//last conditional statement that checks whether
	{							//buffer's first element is a newline character. If so
		if (buffer[0]=='\n')				//the programs continues with its execution. However, 
		{						//buffer's first element is not a newline character
			; 					//program termininates. This last conditional is a 
		}						//check so that only two types of operations are 
		else						//allowed to add and remove data from the stack
		{						//and queue data structures 
			printf("Operation does not exist"); 
			exit(EXIT_FAILURE); 
		}
	}
	buffer[0]=0; 						//resets the buffer to read in another string
}
fclose(fp);							//closes file when the end of file is reached
print(); 							//helper function that prints to stdout the data
								//stored in both outStack and outQueue
return 0; 							//return normal termination of program
}


int addOperation(char *p, int first, int second)		//helper function that continuosly loops over a 
{								//given string looking for digits

char *ad=p; 							//address of first number is loaded in variable ad
char t[INT_SIZE+1];						//a new char array is created to hold as many digits as
int a=first, b=second;   					//an int type is allowed plus one extra byte for the null
while(*ad)							//character
{
	char ch;
	ch=*ad;  
	if(isdigit(ch) && a<INT_SIZE)				//isdigit function checks to see if character is a digit
	{							//if so the conditionl is entered so long as its a sizeable
		t[a]=ch;					//integer. Each character is entered into the char 
		a++; 						//array. For example, "add567" the array t would contain
	}							//t[0]='5', t[1]='6' and t[2]='7'
	ad++; 							//moves pointer to next location in buffer
}
b = atoi(t);							//atoi function then changes the string input into an int
return b; 							//retruns an int to be pushed/enqueded
}

void print(void)						//helper function that prints out the contents of
{								//outStack and outQueue
printf("outStack:  "); 							
for(int i=1; i<=count; i++)
{
	printf("%d ", deleteOutStack()); 
}
printf("\n"); 	
printf("outQueue:  "); 
for(int i=1;  i<=count; i++)
{
	printf("%d ", deleteOutQueue()); 
} 
printf("\n");  
}


