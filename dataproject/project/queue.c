/*
Project: Stack & Queue Juggler
Programmer: Adam Standke
Course: CSC 111
Professor: Dr. Lee
*/

#include <stdio.h>		//imports C's library of std input/output functions
#include <stdlib.h>		//imports C's std library functions
#include "sqjuggler.h"		//imports header file that contain function prototypes

struct inQueue{			//creates the data structure inQueue
	int data; 		//holds data value when add operation is performed
	struct inQueue *next;	//pointer variable that points to an earlier instance  of inQueue
	struct inQueue *prev; 	//pointer variable that points to a later instance of inQueue
};

struct outQueue{		//creates the data structure outQueue
	int data; 		//holds data value when add operation is performed
	struct outQueue *next;	//pointer variable that points to an earlier instance of outQueue
	struct outQueue *prev; 	//pointer variable that points to a later instance of outQueue
};


struct inQueue *front=NULL; 	//pointer variable that points to the first instance of inQueue
struct inQueue *back=NULL; 	//pointer variable that points to the last instance of inQueue

struct outQueue *front2=NULL; 	//pointer variable that points to the first instance of outQueue
struct outQueue *back2=NULL; 	//pointer variable that points to the last instance of outQueue

BOOL isOutQueueEmpty(void)	//function that checks to see if outQueue linked list has no nodes
{
return front2==NULL; 		//if front pointer variable equals the null address which is zero 
}				// a non-zero value is returned representing true, otherwise zero is returned
int deleteOutQueue(void)	//function that represents a dequeue operation in which the first item is removed
{
struct outQueue *temp; 		//creates another pointer variable of type outQueue(used for temp address storage)
int i; 				//holds value that will be returned when delete operation takes place
if(isOutQueueEmpty())		//calls function to see if outQueue linked list has no nodes 
{
	printf("Error in dequeue: queue is empty"); 	//if true the following error message is printed to stdout
	exit(EXIT_FAILURE); 				//exit function is called terminiating program
}
temp=front2; 			//if outQueue linked list has nodes, first store front2's address into temp
i=temp->data; 			//at this point temp will pointing to the first node in the list and its value is given to i
front2=front2->prev;		//front2 then changes its address to a node before it(or the null address if no node exists)
free(temp); 			//at this point the function free is called to release the memory held by the first node
return i; 			//in the list and then the value is returned back to client(ie dequeued)
}
void addOutQueue(int i)		//function that represnts an enqueue operation in which an item is inserted into a linked
{				//list

struct outQueue *new = malloc(sizeof(struct outQueue)); 	//new memory space is set aside for pointer new
if(new==NULL)							//malloc func returns null;  no memory can be set aside
{
	printf("Error in enqueue: queue is full");		//if true the following error message is printed to stdout
	exit(EXIT_FAILURE); 					//exit function is called terminiating program
}
if(front2==NULL && back2==NULL)	//conditional that checks to see that front2 and back2 don't store an address
{				//if true this means that outQueue has no nodes/elements yet
	new->data=i; 		//the value i is inserted into outQueue's member named data 
	new->next=front2;	//since this will be the first node(ie there are no earlier nodes) new's pointer members 
	new->prev=front2; 	//are both pointing to the null memory address
	front2=new; 		//becuase this is the first node, both front2 and back2 point to the first node
	back2=new; 		//in the outQueue linked list
}
else				//if this conditional is entered a node already exists in outQueue
{				
	new->data=i; 		//the value i is inserted into outQueue's member named data
	new->next=back2; 	//the new node's next member now points to an earilier node in the list
	back2->prev=new; 	//the earlier node in the list now also points to the new node 
	back2=new;		//back2 now stores the address of the later node new, instead of the earlier node in the list
}
}
void addInQueue(int i)		//function that represnts an enqueue operation in which an item is inserted into a linked
{				//list 

struct inQueue *new = malloc(sizeof(struct inQueue)); 		//new memory space is set aside for pointer new
if(new==NULL)							//malloc func returns null; no memory can be set aside
{
	printf("Error in enqueue: queue is full");		//if true the following error message is printed to stdout
	exit(EXIT_FAILURE); 					//exit function is called terminiating program
}
if(front==NULL && back==NULL)	//conditional that checks to see that front and back don't store an address
{				//if true this means that inQueue has no nodes/elements yet
	new->data=i; 		//the value i is inserted into inQueue's member named data
	new->next=front;	//since this will be the first node, new's pointer members are both pointing to the null
	new->prev=front; 	//memory address
	front=new; 		//becuase this is the first node, both front and back point to the firs node
	back=new; 		//in the inQueue linked list
}
else				//if this conditional is entered a node alreay exists in inQueue
{
	new->data=i; 		//the value i is inserted into inQueue's member named data
	new->next=back; 	//the new node's next member now points to an earlier node in the list
	back->prev=new; 	//the earlier node in the list now also points to the new node
	back=new; 		//back now stores the address of the later node new, instead of the earlier node in the list
}
}
int deleteInQueue(void)		//function that represents a dequeue operation in which the first item is removed
{
struct inQueue *temp; 		//creates another pointer variable of type inQueue(used for temp address storage)
int i;				//holds the value that will be returned when delete operation takes place
if(isInQueueEmpty())		//calls function to see if inQueue linked list has no nodes 
{
	printf("Error in dequeue: queue is empty"); 	//if ture the following error message is printed to stdout
	exit(EXIT_FAILURE); 				//exit function is called terminiating program
}
temp=front; 			//if inQueue is not empty, first store front's address into temp
i=temp->data; 			//at this point temp will pointing to the first node in the list and its value is given to i
front=front->prev;		//front then changes its address to a node before it (or null if no node exists)
free(temp); 			//at this point the function free is called to release the memory held by the first node
return i; 			//in the list; value is then returned to client(ie dequeued)
}
BOOL isInQueueEmpty(void)	//function that checks to see if inQueue linked list has no nodes 
{
return front==NULL;		//if front pointer variable equals the null address, which is zero, a non-zero 
}				//value is returned representing true, otherwise zero is returned
