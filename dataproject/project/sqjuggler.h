/*
Project: Stack & Queue Juggler
Programmer: Adam Standke
Course: CSC 111
Professor: Dr. Lee
*/

#ifndef SQJUGGLER_H			//conditional directive that checks to see if  header file is defined
#define SQJUGGLER_H			//directive that actually will define header file 

#define BOOL int			//directive that replaces type int with BOOL identifier
#define TRUE 1				//directive that replaces constant value 1 with TRUE identifier
#define FALSE 0				//directive that replaces constant value 0 with FALSE identifier
#define BUFFER_SIZE 20			//directive that replaces constant value 20 with BUFFER_SIZE identifier
					//this represents the maximum number of bytes that be read in
					//each time from a file
#define INT_SIZE 10			//directive that replaces constant value 10 with INT_SIZE identifier
					//program was written using x86 aritecture and thus an int has values
					//that typically max out at 10 digits

void addInQueue(int i); 		//declaration of addInQueue function that enqueue's value i into the inQueue
int deleteInQueue(void); 		//declaration of deleteInQueue function that dequeue's the first item from inQueue
BOOL isInQueueEmpty(void); 		//declaration of isInQueueEmpty function that determines if inQueue is empty
void addOutQueue(int i);		//declaration of addOutQueue function that enqueue's value i into the outQueue
int deleteOutQueue(void);		//declaration of deleteOutQueue function that dequeue's first item from outQueue
BOOL isOutQueueEmpty(void); 		//declaration of isOutQueueEmpty function that determines if ouQueue is empty
void addInStack(int i); 		//declaration of addInStack function that pushes value i into the inStack
int deleteInStack(void); 		//declaration of deleteInStack function that pops the last item from inStack
BOOL isInStackEmpty(void); 		//declaration of isInStackEmpty function that determines if inStack is empty
void addOutStack(int i);		//declaration of addOutStack function that pushes value i into the outStack
int deleteOutStack(void); 		//declaration of deleteOutStack function that pops the last item from outStack
BOOL isOutStackEmpty(void); 		//declaration of isOutStackEmpty function that determines if outStack is empty

#endif					//ending conditional directive
