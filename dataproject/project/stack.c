/*
Credit for stack implementation:
King, K.N., C Programming: A mondern Approach
2nd ed., W.W. Norton & Co, Inc, 2008
*/

#include <stdio.h>				//imports C's std input/output library functions
#include <stdlib.h>				//imports C's std library functions
#include "sqjuggler.h"				//imports header file

struct inStack{					//structure that represents an inStack data structure
	int data; 				//value that can be inserted into inStack data structure
	struct inStack *next; 			//pointer variable member that can point to another inStack
}; 						//data structure

struct outStack{				//structure that represents a outStack data structure
	int data; 				//value that can be inserted into outStack data structure
	struct outStack *next; 			//pointer variable member that can point to another outStack
}; 						//data Structure

struct inStack *top=NULL; 			//pointer variable that points to the last item in inStack
struct outStack *top2=NULL; 			//pointer variable that points to the last item in outStack

void addInStack(int i)					//function that pushes a value ontop of the stack
{
struct inStack *new=malloc(sizeof(struct inStack)); 	//sets memory space for an inStack type node called new 
if (new==NULL)						//if new points to the null address, no memory space could be set
{
	printf("Error in push: stack is full"); 	//printf displays the following error message to stdout
	exit(EXIT_FAILURE); 				//exit function is called terminiating program
}
new->data=i; 					//value of i is inserted into new's member variable called data
new->next=top; 					//new's member variable is set to store top's address (which could be null)
top=new; 					//top then assigned address of new(ie points to last node in list)
}
int deleteInStack(void)				//function that pops a value from the top of the stack
{
struct inStack *old_top; 			//creates a local variable named old_top of inStack type
int i; 						//variable that stores value that will be returned from inStack
if (isInStackEmpty())				//function that checks to see if inStack is empty
{
	printf("Error in pop: stack is empty"); 	//if so, the following error message is printed to stdout
	exit(EXIT_FAILURE); 				//exit function is called terminiating program
}
old_top=top; 					//top's stored address is assigned to old_top
i=top->data; 					//value is stored in i from the top  node in the stack
top=top->next; 					//top's stored address changes to a lower node in the stack
free(old_top); 					//free function is called to clean up memory space and deletes the highest 
return i; 					//node from the list and returns the value stored by the deleted node
}
BOOL isInStackEmpty(void)			//function that determines if inStack data structure contains no nodes
{
return top==NULL; 				//if top pointer does not contain a valid memory address other than the null
						//address zero  is returned signifying false, otherwise true is
}						//returned
BOOL isOutStackEmpty(void)			//function that determines if outStack data structure contains no nodes
{
return top2==NULL; 				//if top2 pointer does not contain a valid memory address other than the
}						//null address zero is returned signifying false, otherwise true is returned 
void addOutStack(int i)				//function that pushes a value ontop of the stack
{
struct outStack *new=malloc(sizeof(struct outStack)); 	//sets memory space for an outStack type node called new
if (new==NULL)						//if new points to the null address space, indicates no memory 
{							//could be set
	printf("Error in push: stack is full"); 	//printf displays the following error message to stdout
	exit(EXIT_FAILURE); 				//exit function is called terminiating program
}
new->data=i; 						//value of i is inserted into new's member variable called data 
new->next=top2; 					//new's member variable is set to store top2's address
top2=new; 						//top2 is assigned address of new(ie points to last node in list)
}
int deleteOutStack(void)			//function that pops a value from the top of the stack
{
struct outStack *old_top; 			//creates a local variable named old_top of outStack type
int i;						//variable that stores value that will be returned from outStack
if(isOutStackEmpty())				//function that checks to see if outStack is empty
{
	printf("Error in pop: stack is empty"); //if so, the following error message is printed to stdout
	exit(EXIT_FAILURE); 			//exit function is called terminating program
}
old_top=top2;					//top2's stored address is assigned to old_top 
i=top2->data; 					//value is tored in i from the top node in the stack
top2=top2->next; 				//top2's stored address changes to a lower node in the stack
free(old_top); 					//free function is called to clean up memory space and deletes the node
return i; 					//from the list and returns the value stored by the deleted node
}
